Rails.application.routes.draw do
  resources :permissions

  get 'presence/sync'

  get 'presence/report'

  get 'presence/details'

  get 'presence/stats'

  resources :machines

  get 'fingerprint/check'

  get 'fingerprint/syncronize'

  resources :time_tables

  resources :holidays

  get 'static_pages/dashboard'

  get 'static_pages/help'

  resources :divisions
  get 'add_division'        => 'divisions#add_division'
  post 'create_division'    => 'divisions#create_division'
  delete 'remove_division/:employee_id'  => 'division#remove_division'
  resources :letters

  mount Bootsy::Engine => '/bootsy', as: 'bootsy'
  
  resources :news

  get 'sessions/new'

  get 'users/new'

  resources :entries

  resources :func_positions

  resources :inpassings

  resources :certificates

  resources :units

  resources :certifications

  resources :ranks

  resources :education_levels

  # resources :educations

  resources :groups

  resources :positions

  resources :users


  # devise_for :users
  resources :employees do
    resources :educations
  end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"

  root                        'static_pages#dashboard'
  get 'help'              =>  'static_pages#help'
  # get 'presence'          =>  'static_pages#presence'
  get 'synclog'           =>  'static_pages#synclog'
  get 'synclog/:id'       =>  'static_pages#show_synclog',    as: :showlog
  # get 'report'  =>  'static_pages#presence_report',    as: :report

  # presence
  get 'presence'          =>  'presence#index'
  get 'report'            =>  'presence#report'
  get 'details'           =>  'presence#details'
  get 'stats'             =>  'presence#stats'
  get 'self'              =>  'presence#self'


  # fingerprint
  get 'checkfp'           => 'fingerprint#check'
  get 'syncronize'        => 'fingerprint#syncronize'

  get   'signup'          =>  'users#new'
  get    'login'          =>  'sessions#new'
  post   'login'          =>  'sessions#create'
  delete 'logout'         =>  'sessions#destroy'

  get 'educations'        =>  'educations#all'

  # devise_for :users, controllers: {
  #   sessions: 'users/sessions',
  #   registrations: 'users/registrations'
  # }
  # get 'users' => 'users#index'
  
  # match 'users', to: 'registrations#index', via: 'get'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
