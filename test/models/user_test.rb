require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new(name: "Muslih",password: "kadaingat",employee_id:1,password_confirmation: "kadaingat")
  end
  
  test "Harus valid nih!" do
  	assert @user.valid?
  end

  test "kolom nama harus ada!" do
    @user.name = "     "
    assert_not @user.valid?
  end

  test "kolom nama jangan diisi terlalu panjang" do
  	@user.name = "a"*51
  	assert_not @user.valid?
  end

  test "kolon pegawai harus diisi" do
    @user.employee_id = " " * 6
    assert_not @user.valid?
  end

  test "user tidak boleh terduplikat" do
  	duplicate_user = @user.dup 
  	duplicate_user.name = @user.name
  	@user.save
  	assert_not duplicate_user.valid?
  end

  test "password harus diisi (tidak kosong)" do
    @user.password = @user.password_confirmation = " " * 6
    assert_not @user.valid?
  end

  test "pasword memiliki minimal karakter" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end

  test "kolom pegawai harus ada" do
    @user.employee_id = "     "
    assert_not @user.valid?
  end
end
