require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest
	# fixtures :employees
	def setup
    @user = users(:one)
  end

  test "login dengan data yang salah" do
    get login_path
    assert_template 'sessions/new'
    post login_path, session: { name: "", password: "" }
    assert_template 'sessions/new'
    assert_not flash.empty?
    get root_path
    assert flash.empty?
  end

  test "login dengan informasi yg benar" do
    get login_path
    post login_path, session: { name: @user.name, password: 'password' }
    assert_redirected_to 'employee/index'
    # get root_path
    # follow_redirect!
    assert_template 'employee/index'
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", logout_path
    # assert_select "a[href=?]", user_path(@user)
  end
end
