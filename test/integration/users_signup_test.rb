require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  # fixtures :employees
  test "notif gagal tambah user" do
    get signup_path
    assert_no_difference 'User.count' do
      post users_path, user: { name:  "",
      						   					 employee_id: "",
                               password:              "foo",
                               password_confirmation: "bar" }
    end
    assert_template 'users/new'
    assert_select 'div.alert.alert-danger.alert-dismissible'
    # assert_select 'div.field_with_errors'
  end

  test "informasi sukses tambah user" do
    get signup_path
    assert_difference 'User.count', 1 do
      post_via_redirect users_path, user: { name:  "Example User",
                                            employee_id:"1",
                                            password:              "password",
                                            password_confirmation: "password" }
    end
    assert_template 'users/new'
    assert is_logged_in?
    
  end
end
