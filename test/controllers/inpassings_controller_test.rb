require 'test_helper'

class InpassingsControllerTest < ActionController::TestCase
  setup do
    @inpassing = inpassings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:inpassings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create inpassing" do
    assert_difference('Inpassing.count') do
      post :create, inpassing: { employee_id: @inpassing.employee_id, func_position_id: @inpassing.func_position_id, lecture_number: @inpassing.lecture_number, number: @inpassing.number }
    end

    assert_redirected_to inpassing_path(assigns(:inpassing))
  end

  test "should show inpassing" do
    get :show, id: @inpassing
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @inpassing
    assert_response :success
  end

  test "should update inpassing" do
    patch :update, id: @inpassing, inpassing: { employee_id: @inpassing.employee_id, func_position_id: @inpassing.func_position_id, lecture_number: @inpassing.lecture_number, number: @inpassing.number }
    assert_redirected_to inpassing_path(assigns(:inpassing))
  end

  test "should destroy inpassing" do
    assert_difference('Inpassing.count', -1) do
      delete :destroy, id: @inpassing
    end

    assert_redirected_to inpassings_path
  end
end
