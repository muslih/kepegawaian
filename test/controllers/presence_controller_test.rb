require 'test_helper'

class PresenceControllerTest < ActionController::TestCase
  test "should get sync" do
    get :sync
    assert_response :success
  end

  test "should get report" do
    get :report
    assert_response :success
  end

  test "should get details" do
    get :details
    assert_response :success
  end

  test "should get stats" do
    get :stats
    assert_response :success
  end

end
