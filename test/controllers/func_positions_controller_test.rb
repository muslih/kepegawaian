require 'test_helper'

class FuncPositionsControllerTest < ActionController::TestCase
  setup do
    @func_position = func_positions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:func_positions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create func_position" do
    assert_difference('FuncPosition.count') do
      post :create, func_position: { name: @func_position.name }
    end

    assert_redirected_to func_position_path(assigns(:func_position))
  end

  test "should show func_position" do
    get :show, id: @func_position
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @func_position
    assert_response :success
  end

  test "should update func_position" do
    patch :update, id: @func_position, func_position: { name: @func_position.name }
    assert_redirected_to func_position_path(assigns(:func_position))
  end

  test "should destroy func_position" do
    assert_difference('FuncPosition.count', -1) do
      delete :destroy, id: @func_position
    end

    assert_redirected_to func_positions_path
  end
end
