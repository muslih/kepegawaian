class Certificate < ActiveRecord::Base
	mount_uploader :document, DocumentUploader
  
  	validates :theme, :presence=> {:message=> "kolom tema harus diisi"}
  	validates :year,:presence=>{:message=> "kolom tahun harus diisi"}
	belongs_to :employee

	def folder_name
		"sertifikat"
	end
end
