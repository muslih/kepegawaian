class Group < ActiveRecord::Base
	has_many :employees
	has_many :ranks
end
