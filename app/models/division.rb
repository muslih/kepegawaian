class Division < ActiveRecord::Base
	validates :name, 	presence: {message:"Kolom nama divisi harus diisi"}
	has_many :employees
	def folder_name
		"divisi"
	end
end
