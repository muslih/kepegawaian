class Certification < ActiveRecord::Base
	mount_uploader :document, DocumentUploader
	belongs_to :employee

	validates :employee_id,:presence => {:message=>"Kolom data pegawai harus diisi"}
	validates :certificate_number ,:presence  =>{:message=>"Kolom nomor sertifikat harus diisi"}
	validates :reg_number,:presence => {:message=>"Kolom nomor registrasi harus diisi"}
	validates :year,:presence => {:message=>"Kolom tanggal harus diisi"}
	# validates :document,:presence => {:message=>"Kolom dokumen harus diisi"}
	def folder_name
		"sertifikasi_dosen"
	end
end
