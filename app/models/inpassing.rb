class Inpassing < ActiveRecord::Base
	mount_uploader :document, DocumentUploader

  # t.integer  "employee_id"
  # t.string   "number"
  # t.string   "func_position_id"
  # t.string   "lecture_number"
  
  validates :number, :presence => {:message=>"Nomor SK harus diisi"}
  validates :func_position_id, :presence => {:message=>"Jabatan harus diisi"}
  validates :lecture_number, :presence => {:message=>"NIDN/NIPN harus diisi"}

	belongs_to :employee
	belongs_to :func_position

  def folder_name
    "jabatan_fungsional"
  end
end
