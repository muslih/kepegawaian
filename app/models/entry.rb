# require 'file_size_validator'
# require 'file_size_validator' 
class Entry < ActiveRecord::Base
  mount_uploader :document, DocumentUploader
  validate :image_size_validation, :if => "document?"

  
  validates :document, file_size: { less_than_or_equal_to:2.megabyte }
  # validates_integrity_of :document
  # validates_processing_of :document
  # validates_format_of :document, :with => %r{\.(pdf)$}i, :message => "File format harus pdf"
  # validates :document, file_size: { maximum: 2.megabytes }
  # validates :document, :file_size => { :maximum => 2.megabytes.to_i }
  validates :number, 
    :presence => {:message=>"Dokumen harus diisi"}
  # validates :document, 
  #   :presence => {:message=>"Dokumen harus diisi"}

  belongs_to :employee

   # validates :picture,
  # :file_size => { 
  #   :maximum => 4.megabytes.to_i
  # }
  def image_size_validation
    if document.size > 1.megabytes
      errors.add(:base, "Ukuran file dokumen tidak boleh lebih dari 1 MB")
    end
  end

  def folder_name
    "TMT"
  end
end
