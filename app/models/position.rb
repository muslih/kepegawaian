class Position < ActiveRecord::Base
	# belongs_to :employee
	has_many :employees
	has_many :units
   
   validates :name,uniqueness:{message: "ups... kolom nama jabatan sudar disi, silahkan masukan yang lain"}

	# Position.where(:name=>"Ketua").first.units.last.employee.name
	def self.nama_ketua
      @hasil = self.where(:name=>"Ketua").first.units.last.employee
      @tdepan = "#{@hasil.front_title}. " if @hasil.front_title.present? 
      @tbelakang = ", #{@hasil.back_title}" if @hasil.back_title.present?
      @result ="#{@tdepan}#{@hasil.name.titleize}#{@tbelakang}"
      return @result
   end
   def self.nik_ketua
      @result = self.where(:name=>"Ketua").first.units.last.employee.nik
      return @result
   end
end
