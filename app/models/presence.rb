class Presence < ActiveRecord::Base
	belongs_to :employee
	# attr_reader :find_date,:find_date_division, :find_date_employee,:find_date_division_employee


	def self.find_date(date)
      @result = self.where(:date => date)
      return @result
   end

   def self.find_date_division(date,division)
      @result = self.where(:date => date,:division_id => division)
      return @result
   end

   def self.find_date_employee(date,employee)
      @result = Presence.where(:date => date,:employee_id=>employee)
      if @result.present?
         @id = @result.first.id
         @last = self.find(@id)
         return @last
      else
         return nil
      end
   end

   def self.find_date_division_employee(date,division,employee)
      @result = self.where(:date => date,:division_id => division,:employee_id=>employee)
      return @result
   end
end
