class Rank < ActiveRecord::Base
	mount_uploader :document, DocumentUploader
	belongs_to :employee
	belongs_to :group

  validates :number, :presence => {:message=>"Nomor SK harus diisi"}
  validates :group_id, :presence => {:message=>"Gol/Ruang harus diisi"}
  validates :year, :presence => {:message=>"Tahun harus diisi"}

	def self.employee(employee_id)
      @result = self.where(:employee_id => employee_id)
      return @result.first
   end

  def folder_name
    "pangkat"
  end
end
