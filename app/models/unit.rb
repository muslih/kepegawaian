class Unit < ActiveRecord::Base
	mount_uploader :document, DocumentUploader
	belongs_to :employee
	belongs_to :position

	validates :employee_id,:presence => {:message=>"Kolom data pegawai harus diisi"}
	validates :position_id,:presence => {:message=>"Kolom jabatan harus diisi"}
	validates :number,:presence => {:message=>"Kolom no surat keterangan harus diisi"}
	validates :date_start ,:presence  =>{:message=>"Kolom masa jabatan awal harus diisi"}
	validates :date_end,:presence => {:message=>"Kolom masa jabatan akhir registrasi harus diisi"}
	
	def self.employee(employee_id)
      @result = self.where(:employee_id => employee_id)
      return @result.first
   end
   def folder_name
		"unit_kerja"
	end
end
