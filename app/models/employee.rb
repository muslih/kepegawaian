class Employee < ActiveRecord::Base
	mount_uploader :picture, PictureUploader
	mount_uploader :id_pic, PictureUploader
	mount_uploader :fam_pic, PictureUploader

	validates :nik, 	presence: {message:"Kolom NIK harus diisi"},
						uniqueness: { case_sensitive: false,message: "kolom NIK sudah terdaftar, silahkan masukan NIK yang lain"}

	validates :name, 	presence: {message:"Kolom nama harus diisi"}, 
						uniqueness: { case_sensitive: false,message: "kolom nama sudah terdaftar, silahkan pilih user yang lain"}

	# validates :id_num, 	presence: {message:"Kolom nomor KTP harus diisi"}, 
						# uniqueness: { case_sensitive: false,message: "kolom nomor KTP sudah terdaftar, silahkan pilih no KTP yang lain"}
	
	# validates :mail, 	presence: {message:"Kolom nomor email harus diisi"}, 
	# 					uniqueness: { case_sensitive: false,message: "kolom email sudah terdaftar, silahkan pilih email yang lain"}
	has_one  :user
	has_many :entries
	has_many :educations
	has_many :ranks
	has_many :certifications
	has_many :units
	has_many :certificates
	has_many :inpassings
	has_many :news
	has_many :syncronizes
	has_many :synces
	has_many :presences
	has_many :permissions

	belongs_to :division
	# mount_uploader :picture, PictureUploader

	before_save :huruf_kecil

	def huruf_kecil
		self.name = self.name.downcase
		# self.birhtplace = self.birhtplace.downcase
	end

	def self.find_division(division)
      @result = self.where(:division_id => division)
      return @result
   end

  def barcode
    @style = "<style media=\"all\" type=\"text/css\">
      table.barby-barcode { border-spacing:0;float:right;margin-bottom:15px}
      tr.barby-row {}
      td.barby-cell { width: 3px !important; height: 50px !important; }
      td.barby-cell.on { background: #000 !important; }</style>"
    @code = self.nik
    @generate_code = Barby::Code128.new(@code)
    @barcode_html = Barby::HtmlOutputter.new(@generate_code)
    @output = @style + @barcode_html.to_html
    @output.html_safe
  end
	

	# has_one :group
  	# has_one :position

end
