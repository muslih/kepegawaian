class User < ActiveRecord::Base
	attr_accessor :remember_token

	before_save {self.name = name.downcase}
	validates :name, 	presence: {message:"Kolom nama harus diisi"}, 
						uniqueness: { case_sensitive: false,message: "kolom username sudah terdaftar, silahkan pilih user yang lain"},
						length: { maximum: 50,message: "Maksimum 50 karater" }
	validates :employee_id, uniqueness: { message: "kolom pegawai sudah terdaftar, silahkan pilih pegawai yang lain" },presence:{message: "kolom pegawai harus diisi"}, allow_nil: true
	
	validates :password,presence:{message: "kolom password harus diisi"}, length:{minimum: 6 ,message: "kolom password minimal 6 digit"}, allow_nil: true
	validates :password_confirmation,presence:{message: "kolom konfirmasi password harus diisi"} #, allow_nil: true
	has_secure_password
	belongs_to :employee

	def User.digest(string)
		cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
		                                              BCrypt::Engine.cost
		BCrypt::Password.create(string, cost: cost)
	end

	def User.new_token
    	SecureRandom.urlsafe_base64
 	end

  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  def authenticated?(remember_token)
  	return false if remember_digest.nil?
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end

  def forget
    update_attribute(:remember_digest, nil)
  end
end
