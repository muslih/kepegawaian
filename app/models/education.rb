class Education < ActiveRecord::Base
	mount_uploader :document, DocumentUploader

 # t.string   "number"
 #    t.string   "graduate"
 #    t.date     "year"
 #    t.integer  "employee_id"
 #    t.datetime "created_at"
 #    t.datetime "updated_at"
 #    t.string   "document"
 #    t.integer  "education_level_id"
 #    t.string   "departement"

  validates :education_level_id, :presence => {:message=>"Jenjang pendidikan harus diisi"}
  validates :number, :presence => {:message=>"Nomor surat perjanjian dinas harus diisi"}
  validates :graduate, :presence => {:message=>"Nama unit pendidikan harus diisi"}
  validates :departement, :presence => {:message=>"Jurusan/program harus diisi"}

	belongs_to :employee
	belongs_to :education_level
	def folder_name
		"pendidikan"
	end
end


