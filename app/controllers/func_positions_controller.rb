class FuncPositionsController < ApplicationController
  before_action :set_func_position, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @func_positions = FuncPosition.all
    respond_with(@func_positions)
  end

  def show
    respond_with(@func_position)
  end

  def new
    @func_position = FuncPosition.new
    respond_with(@func_position)
  end

  def edit
  end

  def create
    @func_position = FuncPosition.new(func_position_params)
    @func_position.save
    respond_with(@func_position)
  end

  def update
    @func_position.update(func_position_params)
    respond_with(@func_position)
  end

  def destroy
    @func_position.destroy
    respond_with(@func_position)
  end

  private
    def set_func_position
      @func_position = FuncPosition.find(params[:id])
    end

    def func_position_params
      params.require(:func_position).permit(:name)
    end
end
