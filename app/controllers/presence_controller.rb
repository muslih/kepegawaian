class PresenceController < ApplicationController
	before_action :logged_in_user
  before_action :admin_user, except:[:self,:details]
  layout proc { false if request.xhr? }
	def index
	end

  def sync
  end

  def report
  	# sleep 1.5
    @start =  params[:report][:start].to_date
    @end = params[:report][:end].to_date
    @range = @start..@end

    @rangein = TimeTable.first.begining_in.strftime('%H:%M')..TimeTable.first.endding_in.strftime('%H:%M')
    @rangeout = TimeTable.first.begining_out.strftime('%H:%M')..TimeTable.first.ending_out.strftime('%H:%M')
  end

  def details
  	@employee = params[:report][:employee_id]
  	@start =  params[:report][:start].to_date
    @end = params[:report][:end].to_date
    @range = @start..@end
    @pegawai = Employee.find(@employee)

    @rangein = (TimeTable.first.begining_in.strftime('%H:%M'))..(TimeTable.first.endding_in.strftime('%H:%M'))
    @rangeout = (TimeTable.first.begining_out.strftime('%H:%M'))..(TimeTable.first.ending_out.strftime('%H:%M'))

    @stats = []
    @range.each do |tgl|
      @absen = Presence.find_date_employee(tgl,@employee)
      @in = ""
      @out= ""

      if @absen.present?
        @in << @absen.in.strftime('%H:%M')
        @out << @absen.out.strftime('%H:%M')
      else
        @in = "00:00"
        @out= "00:00"
      end

      @stats << {:tanggal=>tgl.strftime('%Y-%m-%e'),:in=> @in,:out => @out}
    end
  end

  def stats
    @start =  params[:report][:start].to_date
    @end = params[:report][:end].to_date
    @range = @start..@end

    @stats =[]
    @rangein = (TimeTable.first.begining_in.strftime('%H:%M'))..(TimeTable.first.endding_in.strftime('%H:%M'))
    @rangeout = (TimeTable.first.begining_out.strftime('%H:%M'))..(TimeTable.first.ending_out.strftime('%H:%M'))
    

    @range.each do |tgl|

      # get presence by date
      @absensi = Presence.where(:date=>tgl)      
      @in = []
      @latein = []
      @out = []
      @lateout = []

      # loop each presence
      @absensi.each do |absen|
        # if on time add to in array
        if @rangein === absen.in.strftime('%H:%M') 
          @in << 1
        else
          @latein << 1
        end

        # if ontime add to out array
        if @rangeout === absen.out.strftime('%H:%M') 
          @out << 1
        else
          @lateout << 1
        end
      end
      # push to stats array
      @stats << {:tanggal=>tgl.strftime('%Y-%m-%e'),:in=> @in.count,:latein=> @latein.count,:out => @out.count,:lateout =>@lateout.count}
    end


  end

  def self
  end
end
