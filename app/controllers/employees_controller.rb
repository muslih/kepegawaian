class EmployeesController < ApplicationController
  before_action :logged_in_user
  before_action :set_employee, only: [:show, :edit, :update, :destroy]

  respond_to :html
  
  def index
    # @employees = Employee.all
    if params[:q]
      @employees = Employee.where("name LIKE ? OR nik LIKE ?","%#{params[:q]}%","%#{params[:q]}%")if params[:q]
    
    else
      @employees = Employee.all
    end
    respond_with(@employees)
  end

  def show
    @title = "#{@employee.name.titleize}-#{@employee.nik}"
    # respond_with(@employee)
  end

  def new
    @employee = Employee.new
    respond_with(@employee)
  end

  def edit
  end

  def create
    @employee = Employee.new(employee_params)
    if @employee.save
      flash[:success]= "Data pegawai berhasil di tambah"
      redirect_to action: "index"
    else
      respond_with(@employee)
    end
    # redirect_to action: "index"
  end

  def update
    @employee.update(employee_params)
    respond_with(@employee)
  end

  def destroy
    @employee.destroy
    respond_with(@employee)
  end

  private
    def set_employee
      @employee = Employee.find(params[:id])
    end

    def employee_params
      params.require(:employee).permit(:nik,:name, :birhtplace, :birthdate, :phone, :mobile, :mail, :status,:picture,:id_num,:id_pic,:fam_pic,:address,:front_title,:back_title,:group_id,:position_id,:division_id,:admin)
    end
end
