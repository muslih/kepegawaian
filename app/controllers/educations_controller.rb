class EducationsController < ApplicationController
  before_action :set_education,:set_employee, only: [:show, :edit, :update, :destroy]

  respond_to :html
  respond_to :js

  def index
    # @educations = Education.all
    @employee = Employee.find(params[:employee_id])
    @educations = @employee.educations

    respond_with(@educations)
  end

  def all
    @educations = Education.all
  end

  def show
    respond_with(@education)
  end

  def new
    @education = Education.new
    @employee = Employee.find(params[:employee_id])
    # respond_with(@education)
    respond_to do |format|
      format.html { respond_with(@education) }
      format.js #create.js.erb
    end
  end

  def edit
     @employee = Employee.find(params[:employee_id])
  end

  def create
    # @education = Education.new(education_params)
    # @education.save
    # respond_with(@education)
    @employee = Employee.find(params[:employee_id])
    @education = @employee.educations.create(education_params)
    @no = @employee.educations.count
    respond_to do |format|
      if @education.save
        format.html { redirect_to @employee, notice: 'Riwayat pendidikan berhasil ditambah' }
        format.json { render action: 'show', status: :created, location: @education }
        format.js #create.js.erb
      else
        format.html { render action: 'new' }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  def update
    @education.update(education_params)
    # respond_with(@education)
  end

  def destroy
    
    # respond_with(@education)
    respond_to do |format|
      if @education.destroy
        format.html { redirect_to @education.employee, notice: 'Riwayat pendidikan berhasil dihapus' }
        format.json { render action: 'show', status: :created, location: @education }
        format.js #create.js.erb
      else
        format.html { render action: 'new' }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  private
    def set_education
      @education = Education.find(params[:id])
    end

    def set_employee
      @empleoyee = Employee.find(params[:employee_id])
    end

    def education_params
      params.require(:education).permit(:number, :graduate, :year,:employee_id,:document,:education_level_id,:departement)
    end
end
