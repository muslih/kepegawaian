class UnitsController < ApplicationController
  before_action :set_unit, only: [:show, :edit, :update, :destroy]

  respond_to :html
  respond_to :js

  def index
    @units = Unit.all
    respond_with(@units)
  end

  def show
    respond_with(@unit)
  end

  def new
    @title = "Tambah data unit kerja"
    @unit = Unit.new

    respond_with(@unit)
  end

  def edit
    @title = "Edit data unit kerja"
  end

  def create
    @unit = Unit.new(unit_params)
    
    respond_to do |format|
      if @unit.save
        flash[:success] = "Data berhasil ditambah"
        format.html { redirect_to action: "index" }
        format.json { render action: 'index', status: :created, location: @unit }
        format.js
      else 
        flash[:success] = "Data berhasil ditambah"
        format.html { respond_with(@unit) }
        format.js
      end
    end
  end

  def update
    flash[:success] = "Data berhasil diupdate"
    
    # respond_with(@unit)
    respond_to do |format|
      if @unit.update(unit_params)
        flash[:success] = "Data berhasil diupdate"
        format.html { redirect_to action: "index" }
        format.json { render action: 'index', status: :created, location: @unit }
        format.js
      else 
        flash[:error] = "Data gagal diupdate"
        format.html { respond_with(@unit) }
        format.js
      end
    end
  end

  def destroy
    flash[:success] = "Data berhasil dihapus"
    @unit.destroy
    respond_with(@unit)
  end

  private
    def set_unit
      @unit = Unit.find(params[:id])
    end

    def unit_params
      params.require(:unit).permit(:position_id, :number, :date_start, :date_end, :document,:employee_id)
    end
end
