class StaticPagesController < ApplicationController
  # before_action :set_locale
  before_action :logged_in_user
  before_action :set_logdata, only: [:show_synclog]
  before_action :admin_user,except: [:dashboard,:help] 
  layout proc { false if request.xhr? }
  
# def set_locale
  #   I18n.locale = params[:locale] || I18n.default_locale
  # end
  def dashboard

    today = Date.today # Today's date

    # stats
    @range = today.at_beginning_of_week..today.at_end_of_week-2

    @stats =[]
    @rangein = (TimeTable.first.begining_in.strftime('%H:%M'))..(TimeTable.first.endding_in.strftime('%H:%M'))
    @rangeout = (TimeTable.first.begining_out.strftime('%H:%M'))..(TimeTable.first.ending_out.strftime('%H:%M'))

    @range.each do |tgl|
      # get presence by date
      @absensi = Presence.where(:date=>tgl)      
      @in = []
      @latein = []
      @out = []
      @lateout = []

      # loop each presence
      @absensi.each do |absen|
        # if on time add to in array
        if @rangein === absen.in.strftime('%H:%M') 
          @in << 1
        else
          @latein << 1
        end

        # if ontime add to out array
        if @rangeout === absen.out.strftime('%H:%M') 
          @out << 1
        else
          @lateout << 1
        end
      end
      # push to stats array
      @stats << {:tanggal=>tgl.strftime('%Y-%m-%e'),:in=> @in.count,:latein=> @latein.count,:out => @out.count,:lateout =>@lateout.count}
    end


  	date = DateTime.now.utc
    @days_from_this_week = (today.at_beginning_of_week..today.at_end_of_week-2).map

    @rangein = TimeTable.first.begining_in.strftime('%H:%M')..TimeTable.first.endding_in.strftime('%H:%M')
    @rangeout = TimeTable.first.begining_out.strftime('%H:%M')..TimeTable.first.ending_out.strftime('%H:%M')
    # bulan ini
    @news = News.where('created_at >= ? and created_at <= ?', date.beginning_of_month, date.utc.end_of_month).order("created_at DESC").first(5)
  end

  def help
  end

  def synclog
  end

  def show_synclog
  end

  private
  def set_logdata
    @log = Sync.find(params[:id])
  end

  def report_params
    params.require(:report).permit(:start,:end,:employee,:division)
  end
end

