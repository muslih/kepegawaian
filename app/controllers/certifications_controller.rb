class CertificationsController < ApplicationController
  before_action :set_certification, only: [:show, :edit, :update, :destroy]

  respond_to :html
  respond_to :js

  def index
    @certifications = Certification.all
    respond_with(@certifications)
  end

  def show
    respond_with(@certification)
  end

  def new
    @title = "Tambah Sertifikasi"
    @certification = Certification.new
    respond_with(@certification)
  end

  def edit
    @title = "Edit Sertifikasi"
  end

  def create
    @certification = Certification.new(certification_params)
    
    respond_to do |format|
      if @certification.save
        format.html { redirect_to action: "index" , notice: 'Data berhasil ditambah' }
        format.json { render action: 'index', status: :created, location: @certification }
        format.js
      else 
        format.html { respond_with(@certification) }
        format.js
      end
    end
  end

  def update
    respond_to do |format|
      if @certification.update(certification_params)
        format.html { redirect_to action: "index" , notice: 'Data berhasil diupdate' }
        format.json { render action: 'index', status: :created, location: @certification }
        format.js
      else
        format.html { render action: 'new' }
        format.js
      end
    end
    # respond_with(@certification)
  end

  def destroy
    @certification.destroy
    respond_with(@certification)
  end

  private
    def set_certification
      @certification = Certification.find(params[:id])
    end

    def certification_params
      params.require(:certification).permit(:employee_id, :certificate_number, :reg_number, :year,:document)
    end
end
