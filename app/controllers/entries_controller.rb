class EntriesController < ApplicationController
  before_action :set_entry, only: [:show, :edit, :update, :destroy]

  respond_to :html
  respond_to :js

  def index
    @entries = Entry.all
    respond_with(@entries)
  end

  def show
    respond_with(@entry)
  end

  def new
    @title = "Tambah data TMT"
    @entry = Entry.new
    respond_with(@entry)
  end

  def edit
    @title = "Edit data TMT"
    
  end

  def create
    @entry = Entry.new(entry_params)
    
    # respond_with(@entry)
    # 
    # if @reservation.save
    #   format.html do
    #     redirect_to '/'
    #   end
    #   format.json { render json: @reservation.to_json }
    # else
    #   format.html { render 'new'} ## Specify the format in which you are rendering "new" page
    #   format.json { render json: @reservation.errors } ## You might want to specify a json format as well
    # end
    # -----
    respond_to do |format|
      if @entry.save
        format.html do 
          flash[:success] = "Data berhasil ditambah"
          redirect_to action: "index" 
        end
        format.json {render action: 'index', status: :created, location: @entry}
        format.js
      else
        format.html do 
          flash[:danger] = "Data gagal ditambah"
          render 'new'
        end
        format.json {render json: @entry.errors}
        format.js
      end
    end
  end

  def update
    # respond_with(@entry)
    respond_to do |format|
      if @entry.update(entry_params)
        format.html do
          flash[:success] = "Data berhasil diupdate"
          redirect_to action: "index" 
        end
        format.json {render action: 'index', status: :created, location: @entry }
        format.js
      else
        format.html do
          flash[:danger] = "Data gagal diupdate"
          render 'new'
          # redirect_to action: "index" 
        end
        format.json {render action: 'index', status: :created, location: @entry }
        format.js
      end
    end
  end

  def destroy
    @entry.destroy
    respond_with(@entry)
  end

  private
    def set_entry
      @entry = Entry.find(params[:id])
    end

    def entry_params
      params.require(:entry).permit(:employee_id, :fixed, :contract, :number, :document)
    end
end
