class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  
  protect_from_forgery with: :exception
  
  include SessionsHelper

  def logged_in_user
    unless logged_in?
      flash[:danger] = "Silahkan log in."
      redirect_to login_url
    end
  end

  def admin_user
    redirect_to(root_url) unless current_user.admin?
  end

  def buat_barcode kode
    @code = kode
    @generate_code = Barby::Code128.new(@code)
    @barcode_html = Barby::HtmlOutputter.new(@generate_code)
  end

end


