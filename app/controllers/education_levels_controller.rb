class EducationLevelsController < ApplicationController
  before_action :set_education_level, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @education_levels = EducationLevel.all
    respond_with(@education_levels)
  end

  def show
    respond_with(@education_level)
  end

  def new
    @education_level = EducationLevel.new
    respond_with(@education_level)
  end

  def edit
  end

  def create
    @education_level = EducationLevel.new(education_level_params)
    @education_level.save
    # respond_with(@education_level)
    redirect_to action: "index"
  end

  def update
    @education_level.update(education_level_params)
    respond_with(@education_level)
  end

  def destroy
    @education_level.destroy
    respond_with(@education_level)
  end

  private
    def set_education_level
      @education_level = EducationLevel.find(params[:id])
    end

    def education_level_params
      params.require(:education_level).permit(:name)
    end
end
