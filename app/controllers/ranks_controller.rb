class RanksController < ApplicationController
  before_action :set_rank, only: [:show, :edit, :update, :destroy]

  respond_to :html
  respond_to :js

  def index
    @ranks = Rank.all
    # respond_with(@ranks)
  end

  def show
    respond_with(@rank)
  end

  def new
    @title = "Tambah data"
    @rank = Rank.new
    respond_with(@rank)
  end

  def edit
    @title = "Edit data"
  end

  def create
    @rank = Rank.new(rank_params)

    respond_to do |format|
      if @rank.save
        format.html { redirect_to action: "index" , notice: 'Data berhasil ditambah' }
        format.json { render action: 'index', status: :created, location: @rank }
        format.js
      else
        format.html { redirect_to action: "index" , notice: 'Data berhasil ditambah' }
        format.json { render action: 'index', status: :created, location: @rank }
        format.js
      end
    end

  end

  def update
    respond_to do |format|
      if @rank.update(rank_params)
        format.html { redirect_to action: "index" , notice: 'Data berhasil ditambah' }
        format.json { render action: 'index', status: :created, location: @rank }
        format.js
      else
        format.html { render action: 'new' }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  def destroy
    @rank.destroy
    respond_with(@rank)
  end

  private
    def set_rank
      @rank = Rank.find(params[:id])
    end

    def rank_params
      params.require(:rank).permit(:number, :group_id, :year, :document,:employee_id)
    end
end
