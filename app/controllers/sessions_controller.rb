class SessionsController < ApplicationController
  layout 'login' ,only: [:new,:create]
  # skip_before_action :verify_authenticity_token
  def new
  end

  def create
  	user = User.find_by(name: params[:session][:name].downcase)
    if user && user.authenticate(params[:session][:password])
    	flash.now[:success] = "Hore, anda berhasil masuk!"
      # Log the user in and redirect to the user's show page.
      log_in user
      params[:session][:remember_me] == '1' ? remember(user) : forget(user)
      remember user
      redirect_to root_path
    else
    	flash.now[:danger] = "Username atau password tidak cocok kak!"
      render 'new'
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end
