class LettersController < ApplicationController
  before_action :set_letter, only: [:show, :edit, :update, :destroy]
  before_action :admin_user, except: [:index,:show]
  # GET /letters
  # GET /letters.json
  def index
    @letters = Letter.all
  end

  # GET /letters/1
  # GET /letters/1.json
  def show
    @nama = current_user.employee.name.titleize 
    @nik = current_user.employee.nik 
    @jabatan = current_user.employee.units.last.position.name if current_user.employee.units.present?
    @gol = current_user.employee.ranks.last.group.name if current_user.employee.ranks.present?
    @tanggal = I18n::localize(Date.today,:format=>"%e %B %Y")
    @ketua = Position.nama_ketua
    @nikketua = Position.nik_ketua

    @str = @letter.format  
    # '{{nama}}': 'Nama',
    # '{{nik}}': 'NIK',
    # '{{gol}}}': 'Golongan',
    # #'{{jabatan}}': 'Jabatan',
    #  '{{tanggal}}': 'Tanggal',
    # #'{{ketua}}': 'Ketua',
    # #'{{nikketua}}': 'NIK Ketua'
  end

  # GET /letters/new
  def new
    @letter = Letter.new
  end

  # GET /letters/1/edit
  def edit
  end

  # POST /letters
  # POST /letters.json
  def create
    @letter = Letter.new(letter_params)

    respond_to do |format|
      if @letter.save
        flash[:success] = "Data surat berhasil ditambah"
        format.html { redirect_to action: "index" }
        format.json { render :show, status: :created, location: @letter }
      else
        format.html { render :new }
        format.json { render json: @letter.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /letters/1
  # PATCH/PUT /letters/1.json
  def update
    respond_to do |format|
      if @letter.update(letter_params)
        flash[:success] = "Data surah berhasil diupdate"
        format.html { redirect_to action: "index" }
        format.json { render :show, status: :ok, location: @letter }
      else
        format.html { render :edit }
        format.json { render json: @letter.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /letters/1
  # DELETE /letters/1.json
  def destroy
    @letter.destroy
    flash[:success] = "Data surat berhasil dihapus"
    respond_to do |format|
      format.html { redirect_to letters_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_letter
      @letter = Letter.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def letter_params
      params.require(:letter).permit(:title, :format, :status)
    end
end
