class CertificatesController < ApplicationController
  before_action :set_certificate, only: [:show, :edit, :update, :destroy]

  respond_to :html
  respond_to :js

  def index
    @certificates = Certificate.all
    respond_with(@certificates)
  end

  def show
    respond_with(@certificate)
  end

  def new
    @title = "Tambah Sertifikat"
    @certificate = Certificate.new
    respond_with(@certificate)
  end

  def edit
    @title = "Edit Sertifikat"
  end

  def create

    @certificate = Certificate.new(certificate_params)
    respond_to do |format|
      if @certificate.save
        flash[:success] = "Data Sertifikat berhasil ditambah"
        format.html { redirect_to action: "index" }
        format.json { render action: 'index', status: :created, location: @unit }
        format.js
      else 
        flash[:error] = "Data Sertifikat gagal ditambah"
        format.html { respond_with(@unit) }
        format.js
      end
    end
  end

  def update
    respond_to do |format|
      if  @certificate.update(certificate_params)
        flash[:success] = "Data Sertifikat berhasil diupdate"
        format.html { redirect_to action: "index" }
        # format.json { render action: 'index', status: :created, location: @unit }
        format.js
      else
        flash[:error] = "Data Sertifikat gagal diupdate"
        format.html {redirect_to action: "index"}
        format.js
      end
    end
    # respond_with(@certificate)
    
  end

  def destroy
    if   @certificate.destroy
      flash[:success] = "Data Sertifikat berhasil dihapus"
    else
      flash[:error] = "Data Sertifikat gagal dihapus"
    end
    respond_with(@certificate)
  end

  private
    def set_certificate
      @certificate = Certificate.find(params[:id])
    end

    def certificate_params
      params.require(:certificate).permit(:employee_id, :theme, :year, :document)
    end
end
