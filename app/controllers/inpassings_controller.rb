class InpassingsController < ApplicationController
  before_action :set_inpassing, only: [:show, :edit, :update, :destroy]

  respond_to :html
  respond_to :js

  def index
    @inpassings = Inpassing.all
    respond_with(@inpassings)
  end

  def show
    respond_with(@inpassing)
  end

  def new
    @title = "Tambah data"
    @inpassing = Inpassing.new
    respond_with(@inpassing)
    # redirect_to action: "index"
  end

  def edit
    @title = "Edit data"
  end

  def create
    @inpassing = Inpassing.new(inpassing_params)

    # respond_with(@inpassing)
    # redirect_to action: "index"
    respond_to do |format|
      if @inpassing.save
        flash[:success] = "Data berhasil ditambah"
        format.html {redirect_to action: "index" }
        format.json { render action: 'index', status: :created, location: @inpassing }
        format.js
      else
        flash[:success] = "Data gagal ditambah"
        format.html {redirect_to action: "index" }
        format.json { render action: 'index', status: :created, location: @raninpassingk }
        format.js
      end
    end
  end

  def update
    # respond_with(@inpassing)
    respond_to do |format|
      if   @inpassing.update(inpassing_params)
        flash[:success] = "Data inpassing berhasil diupdate"
        format.html {redirect_to action: "index" }
        format.json { render action: 'index', status: :created, location: @inpassing }
        format.js
      else
        flash[:danger] = "Data inpassing gagal diupdate"
        format.html {redirect_to action: "index" }
        format.json { render action: 'index', status: :created, location: @raninpassing }
        format.js
      end
    end
  end

  def destroy
    if @inpassing.destroy
      flash[:success] = "Data inpassing berhasil dihapus"
      respond_with(@inpassing)
    end
  end

  private
    def set_inpassing
      @inpassing = Inpassing.find(params[:id])
    end

    def inpassing_params
      params.require(:inpassing).permit(:employee_id, :number, :func_position_id, :lecture_number,:document)
    end
end
