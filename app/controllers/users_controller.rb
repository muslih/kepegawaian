class UsersController < ApplicationController
  before_action :logged_in_user
  before_action :admin_user,     only: :destroy
	def index
		@users = User.all.paginate(page: params[:page],:per_page => 10)
    # @users= User.all
	end
  def show
    @user = User.find(params[:id])
    debugger
  end

  def new
  	@user = User.new
  end

  def create
  	@user = User.new(user_params)    # Not the final implementation!
    if @user.save
      flash[:success] = "Data berhasil disimpan"
      redirect_to action: "index"
    else
    	flash[:danger] = "Data gagal disimpan"
      render 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      # Handle a successful update.
      flash[:success] = "Akun berhasil diubah"
      redirect_to root_path
    else
      render 'edit'
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "Akun berhasil dihapus"
    redirect_to users_url
  end

  private

  def user_params
  	params.require(:user).permit(:name,:employee_id, :password, :password_confirmation)
  end
end
