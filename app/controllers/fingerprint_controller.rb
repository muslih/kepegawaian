class FingerprintController < ApplicationController
  before_action :target
  before_action :key

  layout proc { false if request.xhr? }
  def check
    @xml = %{<Restart><ArgComKey Xsi:type="xsd:integer">#{@key}</ ArgComKey> </ Restart>}
    # @Response = true
    
    connect(@target,@xml,false)
  end

  def syncronize
    # @status = true
    
    # return @response
    
    @xml_data = %{<GetAttLog><ArgComKey xsi:type="xsd:integer">#{@key}</ArgComKey><Arg><PIN xsi:type="xsd:integer">All</PIN></Arg></GetAttLog>}
    connect(@target,@xml_data,true)
    

    @employees = Employee.all
    @daterange = Sync.last.date..Date.today-1
    @result = []
    @log = ""
    # Syncronize if last sync date not today
    if Sync.last.date == Date.today
      @status = false
      @message = "Data Gagal Di singkronisasi karena sudah disingkronisasi pada tanggal yang sama, lihat detil log!"
      @log << ">> Data Sudah di singronisasi sebelumnya"
      @result << ">> NIL"
    else
      @status = true
      # list all employee
      @employees.each do |employee|
        # list all date by range
        @daterange.each do |date|
          # get first and last date
          find_presence(employee.id,@Response,date)
          if @Data.any?
            @result << @Data
            @absensi = Presence.new(:employee_id=>employee.id, :date=>date, :in=> @Data.first['datetime'], :out=> @Data.last['datetime'], :division_id=>employee.division_id)
            if @absensi.save
              @log << "
SUKSES TANGGAL #{date.to_s}
  pegawai   => #{employee.name.titleize}
  masuk     => #{@Data.first['datetime'].to_s}
  keluar    => #{@Data.last['datetime'].to_s}
  divisi    => #{employee.division.name if employee.division_id.present?}<br>"
            else
              @log << "
GAGAL TANGGAL #{date.to_s}
  pegawai   => #{employee.name.titleize}
  masuk     => #{@Data.first['datetime'].to_s}
  keluar    => #{@Data.last['datetime'].to_s}
  divisi    => #{employee.division.name if employee.division_id.present?}<br>"
            end #end @absensi.save
          end #end @Data.any
        # @result << @Data
        end #end @datarange.each
      end #end @employee.each
      Sync.create date:Date.today,employee_id:current_user.id,log:@log.to_s
      @message = "Data Berhasil Di singkronisasi"
    
    end #end Sync.last.date ==Date.today
  end

  private
    def target
      @target ||= Machine.first.ip + Machine.first.path
    end

    def key
      @key ||= Machine.first.key
    end
    # konek data
    def connect(clientpath, xml_data,parse)
      begin
        uri = URI.parse('http://'+clientpath)
        req = Net::HTTP.new(uri.hostname, uri.port)
        # req.read_timeout = 300
        req.read_timeout = 5
        req.open_timeout = 5
        @res = req.post(uri.path, xml_data, {'Content-Type' => 'text/xml'})
        # @res = req.post(uri.path)
        case @res
        when Net::HTTPSuccess
          if parse == true
            # @Response = true
            @Response = @res.body
          else
            @Response = true
          end
        else
          @Response = false
        end
      rescue Timeout::Error => e
        @Response = false
      rescue Errno::ENETUNREACH => e
        @Response = false
      rescue Errno::EHOSTUNREACH => e
        @Response = false
      rescue Errno::ECONNREFUSED => e
        @Response = false
      rescue Errno::EHOSTDOWN => e
        @Response = false
      end
    end

    # cari data berdasarkan id
    def find_presence(id,xml_data,date)
      @doc = Nokogiri::XML(xml_data)
      @Data = []
      @doc.xpath('//Row').each do |zone|
        if zone.xpath('PIN').text == id.to_s  and  zone.xpath('DateTime').text.split(' ').first == date.to_s
          # @Response << { "pin" => zone.xpath('PIN').text, "datetime" => zone.xpath('DateTime').text, "Verified" => zone.xpath("Verified").text, "status" => zone.xpath("Status").text, "workcode" => zone.xpath("WorkCode").text}
            @Data << { "pin" => zone.xpath('PIN').text, "datetime" => zone.xpath('DateTime').text, "workcode" => zone.xpath("WorkCode").text}
        end
      end
    end

  end

class Time
  @timetable = TimeTable.first
  def in?
    hour >= @timetable.begining_in and hour <= @timetable.endding_in
  end
  def out?
     hour >= @timetable.begining_in and hour <= @timetable.ending_in
  end
end
