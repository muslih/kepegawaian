class NewsController < ApplicationController
  before_action :logged_in_user
  before_action :admin_user, except: [:index,:show] 
  before_action :set_news, only: [:show, :edit, :update, :destroy]

  # GET /news
  # GET /news.json
  def index
    @news = News.all.order("created_at DESC")
  end

  # GET /news/1
  # GET /news/1.json
  def show
  end

  # GET /news/new
  def new
    @news = News.new
  end

  # GET /news/1/edit
  def edit
  end

  # POST /news
  # POST /news.json
  def create
    @news = News.new(news_params)

    respond_to do |format|
      if @news.save
        flash[:success] = "Data pengumuman berhasil ditambah"
        format.html { redirect_to action: "index"}
        format.json { render :show, status: :created, location: @news }
      else
        flash.now[:danger] = "Data pengumuman gagal ditambah"
        format.html { render :new }
        format.json { render json: @news.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /news/1
  # PATCH/PUT /news/1.json
  def update
    respond_to do |format|
      if @news.update(news_params)
        flash[:success] = "Data pengumuman berhasil di update"
        format.html { redirect_to action: "index"}
        format.json { render :show, status: :ok, location: @news }
      else
        flash[:danger] = "Data pengumuman gagal diubah"
        format.html { render :edit }
        format.json { render json: @news.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /news/1
  # DELETE /news/1.json
  def destroy
    @news.destroy
    flash.now[:success] = "Data pengumuman berhasil dihapus"
    respond_to do |format|
      format.html { redirect_to action: "index"}
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_news
      @news = News.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def news_params
      params.require(:news).permit(:title, :body, :employee_id)
    end
end
