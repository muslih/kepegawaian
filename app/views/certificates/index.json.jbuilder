json.array!(@certificates) do |certificate|
  json.extract! certificate, :id, :employee_id, :theme, :year, :document
  json.url certificate_url(certificate, format: :json)
end
