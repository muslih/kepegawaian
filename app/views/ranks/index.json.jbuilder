json.array!(@ranks) do |rank|
  json.extract! rank, :id, :number, :group_id, :year, :document
  json.url rank_url(rank, format: :json)
end
