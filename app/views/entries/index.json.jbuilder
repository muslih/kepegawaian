json.array!(@entries) do |entry|
  json.extract! entry, :id, :employee_id, :fixed, :contract, :number, :document
  json.url entry_url(entry, format: :json)
end
