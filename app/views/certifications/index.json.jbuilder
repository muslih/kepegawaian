json.array!(@certifications) do |certification|
  json.extract! certification, :id, :employee_id, :certificate_number, :reg_number, :year
  json.url certification_url(certification, format: :json)
end
