json.array!(@time_tables) do |time_table|
  json.extract! time_table, :id, :begining_in, :endding_in, :begining_out, :ending_out
  json.url time_table_url(time_table, format: :json)
end
