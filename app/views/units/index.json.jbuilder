json.array!(@units) do |unit|
  json.extract! unit, :id, :position_id, :number, :date_start, :date_end, :document
  json.url unit_url(unit, format: :json)
end
