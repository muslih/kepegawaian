json.array!(@permissions) do |permission|
  json.extract! permission, :id, :employee_id, :first, :end, :description
  json.url permission_url(permission, format: :json)
end
