json.array!(@machines) do |machine|
  json.extract! machine, :id, :ip
  json.url machine_url(machine, format: :json)
end
