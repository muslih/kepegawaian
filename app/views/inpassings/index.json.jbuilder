json.array!(@inpassings) do |inpassing|
  json.extract! inpassing, :id, :employee_id, :number, :func_position_id, :lecture_number
  json.url inpassing_url(inpassing, format: :json)
end
