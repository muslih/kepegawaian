json.array!(@educations) do |education|
  json.extract! education, :id, :number, :graduate, :year
  json.url education_url(education, format: :json)
end
