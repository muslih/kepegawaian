json.array!(@func_positions) do |func_position|
  json.extract! func_position, :id, :name
  json.url func_position_url(func_position, format: :json)
end
