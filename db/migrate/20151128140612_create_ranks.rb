class CreateRanks < ActiveRecord::Migration
  def change
    create_table :ranks do |t|
      t.string :number
      t.integer :group_id
      t.date :year
      t.string :document

      t.timestamps
    end
  end
end
