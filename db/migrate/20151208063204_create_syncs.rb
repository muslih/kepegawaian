class CreateSyncs < ActiveRecord::Migration
  def change
    create_table :syncs do |t|
      t.date :date
      t.integer :employee_id

      t.timestamps
    end
  end
end
