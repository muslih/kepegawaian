class AddFixedToEmployees < ActiveRecord::Migration
  def change
    add_column :employees, :fixed, :date
  end
end
