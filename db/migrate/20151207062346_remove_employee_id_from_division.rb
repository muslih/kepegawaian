class RemoveEmployeeIdFromDivision < ActiveRecord::Migration
  def change
    remove_column :divisions, :employee_id, :integer
  end
end
