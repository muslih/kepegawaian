class AddEmployeeIdToUnits < ActiveRecord::Migration
  def change
    add_column :units, :employee_id, :integer
  end
end
