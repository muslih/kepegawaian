class CreateTimeTables < ActiveRecord::Migration
  def change
    create_table :time_tables do |t|
      t.time :begining_in
      t.time :endding_in
      t.time :begining_out
      t.time :ending_out

      t.timestamps
    end
  end
end
