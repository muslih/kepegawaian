class CreateUnits < ActiveRecord::Migration
  def change
    create_table :units do |t|
      t.integer :position_id
      t.string :number
      t.date :date_start
      t.date :date_end
      t.string :document

      t.timestamps
    end
  end
end
