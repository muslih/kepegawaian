class CreateCertifications < ActiveRecord::Migration
  def change
    create_table :certifications do |t|
      t.integer :employee_id
      t.string :certificate_number
      t.string :reg_number
      t.date :year

      t.timestamps
    end
  end
end
