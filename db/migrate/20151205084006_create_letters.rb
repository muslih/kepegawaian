class CreateLetters < ActiveRecord::Migration
  def change
    create_table :letters do |t|
      t.string :title
      t.text :format
      t.boolean :status

      t.timestamps
    end
  end
end
