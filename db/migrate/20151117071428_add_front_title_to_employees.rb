class AddFrontTitleToEmployees < ActiveRecord::Migration
  def change
    add_column :employees, :front_title, :string
  end
end
