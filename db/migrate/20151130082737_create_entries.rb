class CreateEntries < ActiveRecord::Migration
  def change
    create_table :entries do |t|
      t.integer :employee_id
      t.date :fixed
      t.date :contract
      t.string :number
      t.string :document

      t.timestamps
    end
  end
end
