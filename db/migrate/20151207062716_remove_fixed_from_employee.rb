class RemoveFixedFromEmployee < ActiveRecord::Migration
  def change
    remove_column :employees, :fixed, :date
  end
end
