class RemoveGroupIdFromEmployee < ActiveRecord::Migration
  def change
    remove_column :employees, :group_id, :string
  end
end
