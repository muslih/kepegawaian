class AddContractToEmployees < ActiveRecord::Migration
  def change
    add_column :employees, :contract, :date
  end
end
