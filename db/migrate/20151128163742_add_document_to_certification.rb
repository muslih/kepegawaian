class AddDocumentToCertification < ActiveRecord::Migration
  def change
    add_column :certifications, :document, :string
  end
end
