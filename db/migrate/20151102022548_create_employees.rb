class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.string :name
      t.string :birhtplace
      t.date :birthdate
      t.string :phone
      t.string :mobile
      t.string :mail
      t.boolean :status

      t.timestamps
    end
  end
end
