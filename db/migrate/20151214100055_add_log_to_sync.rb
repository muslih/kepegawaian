class AddLogToSync < ActiveRecord::Migration
  def change
    add_column :syncs, :log, :text
  end
end
