class CreatePermissions < ActiveRecord::Migration
  def change
    create_table :permissions do |t|
      t.string :employee_id
      t.date :first
      t.date :end
      t.string :description

      t.timestamps
    end
  end
end
