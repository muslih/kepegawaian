class RemovePositionIdFromEmployee < ActiveRecord::Migration
  def change
    remove_column :employees, :position_id, :integer
  end
end
