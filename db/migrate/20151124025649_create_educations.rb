class CreateEducations < ActiveRecord::Migration
  def change
    create_table :educations do |t|
      t.string :number
      t.string :graduate
      t.date :year
      t.integer :employee_id

      t.timestamps
    end
  end
end
