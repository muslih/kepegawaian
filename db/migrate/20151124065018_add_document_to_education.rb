class AddDocumentToEducation < ActiveRecord::Migration
  def change
    add_column :educations, :document, :string
  end
end
