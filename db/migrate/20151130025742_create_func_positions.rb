class CreateFuncPositions < ActiveRecord::Migration
  def change
    create_table :func_positions do |t|
      t.string :name

      t.timestamps
    end
  end
end
