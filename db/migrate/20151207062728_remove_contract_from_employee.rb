class RemoveContractFromEmployee < ActiveRecord::Migration
  def change
    remove_column :employees, :contract, :date
  end
end
