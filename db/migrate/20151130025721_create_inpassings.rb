class CreateInpassings < ActiveRecord::Migration
  def change
    create_table :inpassings do |t|
      t.integer :employee_id
      t.string :number
      t.string :func_position_id
      t.string :lecture_number

      t.timestamps
    end
  end
end
