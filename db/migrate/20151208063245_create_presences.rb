class CreatePresences < ActiveRecord::Migration
  def change
    create_table :presences do |t|
      t.integer :employee_id
      t.date :date
      t.time :in
      t.time :out
      t.integer :division_id
      t.string :description

      t.timestamps
    end
  end
end
