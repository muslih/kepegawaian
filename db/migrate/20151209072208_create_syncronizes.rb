class CreateSyncronizes < ActiveRecord::Migration
  def change
    create_table :syncronizes do |t|
      t.string :employee_id

      t.timestamps
    end
  end
end
