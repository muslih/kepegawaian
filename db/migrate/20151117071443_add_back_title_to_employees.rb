class AddBackTitleToEmployees < ActiveRecord::Migration
  def change
    add_column :employees, :back_title, :string
  end
end
