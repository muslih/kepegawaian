class CreateCertificates < ActiveRecord::Migration
  def change
    create_table :certificates do |t|
      t.string :employee_id
      t.string :theme
      t.date :year
      t.string :document

      t.timestamps
    end
  end
end
