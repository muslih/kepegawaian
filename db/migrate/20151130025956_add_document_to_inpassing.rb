class AddDocumentToInpassing < ActiveRecord::Migration
  def change
    add_column :inpassings, :document, :string
  end
end
