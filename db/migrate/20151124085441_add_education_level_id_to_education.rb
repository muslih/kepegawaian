class AddEducationLevelIdToEducation < ActiveRecord::Migration
  def change
    add_column :educations, :education_level_id, :integer
  end
end
