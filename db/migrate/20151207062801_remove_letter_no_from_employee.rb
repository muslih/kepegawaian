class RemoveLetterNoFromEmployee < ActiveRecord::Migration
  def change
    remove_column :employees, :letter_no, :string
  end
end
