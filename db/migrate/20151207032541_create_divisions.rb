class CreateDivisions < ActiveRecord::Migration
  def change
    create_table :divisions do |t|
      t.integer :employee_id
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
