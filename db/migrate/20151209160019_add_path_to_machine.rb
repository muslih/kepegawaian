class AddPathToMachine < ActiveRecord::Migration
  def change
    add_column :machines, :path, :string
  end
end
