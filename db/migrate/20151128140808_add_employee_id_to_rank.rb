class AddEmployeeIdToRank < ActiveRecord::Migration
  def change
    add_column :ranks, :employee_id, :integer
  end
end
