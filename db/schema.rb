# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160220030924) do

  create_table "bootsy_image_galleries", force: true do |t|
    t.integer  "bootsy_resource_id"
    t.string   "bootsy_resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bootsy_images", force: true do |t|
    t.string   "image_file"
    t.integer  "image_gallery_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "certificates", force: true do |t|
    t.string   "employee_id"
    t.string   "theme"
    t.date     "year"
    t.string   "document"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "certifications", force: true do |t|
    t.integer  "employee_id"
    t.string   "certificate_number"
    t.string   "reg_number"
    t.date     "year"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "document"
  end

  create_table "divisions", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "education_levels", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "educations", force: true do |t|
    t.string   "number"
    t.string   "graduate"
    t.date     "year"
    t.integer  "employee_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "document"
    t.integer  "education_level_id"
    t.string   "departement"
  end

  create_table "employees", force: true do |t|
    t.string   "name"
    t.string   "birhtplace"
    t.date     "birthdate"
    t.string   "phone"
    t.string   "mobile"
    t.string   "mail"
    t.boolean  "status"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "nik"
    t.string   "id_num"
    t.string   "picture"
    t.string   "id_pic"
    t.string   "fam_pic"
    t.text     "address"
    t.string   "front_title"
    t.string   "back_title"
    t.integer  "division_id"
  end

  create_table "entries", force: true do |t|
    t.integer  "employee_id"
    t.date     "fixed"
    t.date     "contract"
    t.string   "number"
    t.string   "document"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "func_positions", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "groups", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "holidays", force: true do |t|
    t.string   "name"
    t.date     "date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "inpassings", force: true do |t|
    t.integer  "employee_id"
    t.string   "number"
    t.string   "func_position_id"
    t.string   "lecture_number"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "document"
  end

  create_table "letters", force: true do |t|
    t.string   "title"
    t.text     "format"
    t.boolean  "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "machines", force: true do |t|
    t.string   "ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "key"
    t.string   "port"
    t.string   "path"
  end

  create_table "news", force: true do |t|
    t.string   "title"
    t.text     "body"
    t.integer  "employee_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "permissions", force: true do |t|
    t.string   "employee_id"
    t.date     "first"
    t.date     "end"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "positions", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "presences", force: true do |t|
    t.integer  "employee_id"
    t.date     "date"
    t.time     "in"
    t.time     "out"
    t.integer  "division_id"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ranks", force: true do |t|
    t.string   "number"
    t.integer  "group_id"
    t.date     "year"
    t.string   "document"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "employee_id"
  end

  create_table "syncronizes", force: true do |t|
    t.string   "employee_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "syncs", force: true do |t|
    t.date     "date"
    t.integer  "employee_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "log"
  end

  create_table "time_tables", force: true do |t|
    t.time     "begining_in"
    t.time     "endding_in"
    t.time     "begining_out"
    t.time     "ending_out"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "units", force: true do |t|
    t.integer  "position_id"
    t.string   "number"
    t.date     "date_start"
    t.date     "date_end"
    t.string   "document"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "employee_id"
  end

  create_table "users", force: true do |t|
    t.string   "name"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "password_digest"
    t.integer  "employee_id"
    t.string   "remember_digest"
    t.boolean  "admin"
  end

  add_index "users", ["name"], name: "index_users_on_name", unique: true

end
